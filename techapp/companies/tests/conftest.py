import pytest
from companies.models import Company


@pytest.fixture
def amazon():
    return Company.objects.create(name="Amazon")
