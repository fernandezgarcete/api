import json
import pytest

from django.urls import reverse
from companies.models import exception_function, ModelException


companies_url = reverse("companies-list")
pytestmark = pytest.mark.django_db


def test_zero_companies_should_return_empty_list(client):
    response = client.get(companies_url)
    assert response.status_code == 200
    assert json.loads(response.content) == []


def test_one_company_exists_should_succeed(client, amazon):
    company = amazon
    response = client.get(companies_url)
    response_content = json.loads(response.content).pop()
    assert response.status_code == 200
    assert response_content.get("name") == company.name
    assert response_content.get("status") == "Hiring"
    assert response_content.get("application_link") == ""
    assert response_content.get("notes") == ""


def test_model_exception_should_pass():
    with pytest.raises(ModelException) as err:
        exception_function()
    assert str(err.value) == "Model Exception"


@pytest.mark.xfail
def test_should_be_ok_if_fails():
    assert 1 == 2


@pytest.mark.skip
def test_should_be_skipped():
    assert 1 == 2
