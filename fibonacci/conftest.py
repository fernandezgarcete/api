import pytest
from datetime import datetime, timedelta
from typing import Callable


@pytest.fixture
def time_tracker():
    t1 = datetime.now()
    yield
    t2 = datetime.now()
    diff = t2 - t1
    print(f"\n runtime: {diff.total_seconds()}")


class PerformanceException(Exception):
    def __init__(self, runtime: timedelta, limit: timedelta):
        self.runtime = runtime
        self.limit = limit

    def __str__(self) -> str:
        return f"Performance test failed, runtime: {self.runtime.total_seconds()}, limit: {self.limit.total_seconds()}"


def track_performance(method: Callable, runtime_limit=timedelta(seconds=2)):
    def run_function_and_validate_runtime(*args, **kw):
        t1 = datetime.now()
        result = method(*args, **kw)
        t2 = datetime.now()
        runtime = t2 - t1
        print(f"\n runtime: {runtime.total_seconds()}")

        if runtime > runtime_limit:
            raise PerformanceException(runtime=runtime, limit=runtime_limit)

        return result

    return run_function_and_validate_runtime
