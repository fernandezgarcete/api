def fibonacci_naive(n):
    if n < 2:
        return n
    return fibonacci_naive(n - 2) + fibonacci_naive(n - 1)


def fibonacci_cache(n):
    cache = {}
    if n in cache:
        return cache[n]

    if n < 2:
        return n
    fn = fibonacci_naive(n - 2) + fibonacci_naive(n - 1)
    cache[n] = fn

    return fn
