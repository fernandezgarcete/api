import pytest
from fibonacci.fibonacci import fibonacci_naive, fibonacci_cache
from fibonacci.conftest import track_performance


@pytest.mark.parametrize(
    "n, expected",
    [
        (0, 0),
        (1, 1),
        (2, 1),
        (3, 2),
        (4, 3),
    ]
)
@pytest.mark.parametrize(
    "func_fibo",
    [
        (fibonacci_naive),
        (fibonacci_cache),
    ]
)
def test_fibonacci(func_fibo, n, expected, time_tracker):
    result = func_fibo(n=n)
    assert result == expected
