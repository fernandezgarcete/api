from fibonacci.fibonacci import fibonacci_cache
from fibonacci.conftest import track_performance


@track_performance
def fibonacci_timetracker():
    result = fibonacci_cache(n=40)
    assert result == 102334155
